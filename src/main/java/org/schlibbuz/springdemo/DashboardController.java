package org.schlibbuz.springdemo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DashboardController {

    @RequestMapping("/")
    public String dashboard() {
        return "Dashboard";
    }
}
